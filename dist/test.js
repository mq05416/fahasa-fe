// // var test = (function() {
// //     var privateVar = 'a';
// //     var privateMethod = function() {
// //         console.log(privateVar);
// //     };
// //     return {
// //         publicVar: privateVar,
// //         publicMethod: function() {
// //             alert(privateVar);
// //         }
// //     }
// // }());
// // console.log(test.privateVar);
// // console.log(test.publicVar);
// // // test.privateMethod();
// // test.publicMethod();

// // function User() {
// //     // Thuộc tính
// //     this.username = '';
// //     this.password = '';

// //     // Phương thức
// //     this.setInfo = function(username, password) {
// //         this.username = username;
// //         this.password = password;
// //     };

// //     this.checkLogin = function() {
// //         return (this.username === 'admin' && this.password === '@123');
// //     };

// //     // Phải return this thì mới tạo mới được đối tượng
// //     return this;
// // }
// // // Cách sử dụng
// // var user = new User();
// // user.setInfo('admin', '@123');
// // if (user.checkLogin()) {
// //     alert('Đăng nhập thành công');
// // } else {
// //     alert('Đăng nhập thất bại');
// // }

// // Phía trên cùng của file
// // "use strict";

// // Đoạn code này lỗi vì biến domain chưa được khởi tạo
// // domain = "https://freetuts.net";

// // // In ra màn hình
// // document.write(domain);
// 'use strict';
// var a = 'Luan 123';
// var test = (function() {
//     var privateVar = 'a';
//     var privateMethod = function() {
//         console.log(privateVar);
//     };
//     return {
//         publicVar: privateVar,
//         publicMethod: function() {
//             alert(privateVar);
//         }
//     }
// }());

// function hello() {
//     alert("Hello");
// }
// // hello();
// (function hello() {
//     alert("Hello1");
// })();
// (function() {
//     alert("Hello23");
// })();
// (function() {
//     alert("Hello2356");
// }());