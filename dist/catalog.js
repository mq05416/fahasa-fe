var data = {
    "status": 1,
    "message": "Success",
    "parent_categories": [{
            "id": "3165",
            "name": "Foreign books",
            "path": "https://www.fahasa.com/foreigncategory.html",
            "url": "https://www.fahasa.com/foreigncategory.html"
        },
        {
            "id": "5421",
            "name": "Dictionaries & Languages",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages.html",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages.html"
        },
        {
            "id": "5456",
            "name": "Language Teaching & Learning (other Than ELT)",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt.html",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt.html"
        },
        {
            "id": "5458",
            "name": "Language Teaching & Learning Material & Coursework",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework.html",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework.html"
        }
    ],
    "category": {
        "id": "5458",
        "name": "Language Teaching & Learning Material & Coursework",
        "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework.html",
        "title": "Tổng hợp sách Language Teaching & Learning Material & Coursework tại Fahasa.com",
        "description": "Tổng hợp sách Language Teaching & Learning Material & Coursework tại Fahasa.com, với ưu đãi hàng ngày lên tới 50%, giao hàng miễn phí toàn quốc chỉ từ 140k.",
        "keywords": "sách Language Teaching & Learning Material & Coursework, SACH language teaching & learning material & coursework, language teaching & learning material & coursework"
    },
    "children_categories": [{
            "id": "5462",
            "name": "Language Learning: Audio-visual & Multimedia",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-learning-audio-visual-multimedia.html",
            "count": "20",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-learning-audio-visual-multimedia.html"
        },
        {
            "id": "5461",
            "name": "Language Readers",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-readers.html",
            "count": "10",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-readers.html"
        },
        {
            "id": "5460",
            "name": "Grammar & Vocabulary",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/grammar-vocabulary.html",
            "count": "9",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/grammar-vocabulary.html"
        },
        {
            "id": "5459",
            "name": "Language Self-study Texts",
            "path": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-self-study-texts.html",
            "count": "3",
            "url": "https://www.fahasa.com/foreigncategory/dictionaries-languages/language-teaching-learning-other-than-elt/language-teaching-learning-material-coursework/language-self-study-texts.html"
        }
    ],
    "price_range": {
        "price_range": {
            "min": 0,
            "max": 999999999
        },
        "min": 0,
        "max": 999999999
    },
    "attributes": [{
            "id": "75",
            "code": "price",
            "label": "Giá",
            "param": "giá",
            "options": []
        },
        {
            "id": "140",
            "code": "book_layout",
            "label": "Hình thức",
            "param": "hình-thức",
            "options": [{
                    "id": "9",
                    "label": "Bìa Mềm",
                    "selected": false,
                    "param": "bìa-mềm",
                    "count": "189"
                },
                {
                    "id": "120",
                    "label": "Mixed Media Product",
                    "selected": false,
                    "param": "mixed-media-product",
                    "count": "92"
                },
                {
                    "id": "137",
                    "label": "Cards",
                    "selected": false,
                    "param": "cards",
                    "count": "4"
                },
                {
                    "id": "90",
                    "label": "CD/DVD",
                    "selected": false,
                    "param": "cd/dvd",
                    "count": "1"
                },
                {
                    "id": "126",
                    "label": "Bìa Cứng",
                    "selected": false,
                    "param": "bìa-cứng",
                    "count": "1"
                }
            ]
        },
        {
            "id": "194",
            "code": "supplier_list",
            "label": "Nhà Cung Cấp",
            "param": "nhà-cung-cấp",
            "options": [{
                    "id": "860955",
                    "label": "Cambridge University Press",
                    "selected": false,
                    "param": "cambridge-university-press",
                    "count": "65"
                },
                {
                    "id": "861903",
                    "label": "Macmillan Publishers",
                    "selected": false,
                    "param": "macmillan-publishers",
                    "count": "48"
                },
                {
                    "id": "862107",
                    "label": "Oxford University Press",
                    "selected": false,
                    "param": "oxford-university-press",
                    "count": "41"
                },
                {
                    "id": "860963",
                    "label": "Cengage",
                    "selected": false,
                    "param": "cengage",
                    "count": "33"
                },
                {
                    "id": "862111",
                    "label": "Pearson Education",
                    "selected": false,
                    "param": "pearson-education",
                    "count": "20"
                },
                {
                    "id": "861754",
                    "label": "E-Future.Co.,Ltd",
                    "selected": false,
                    "param": "e-future.co.,ltd",
                    "count": "18"
                },
                {
                    "id": "861780",
                    "label": "Hachette Book Group",
                    "selected": false,
                    "param": "hachette-book-group",
                    "count": "16"
                },
                {
                    "id": "862159",
                    "label": "Richmond",
                    "selected": false,
                    "param": "richmond",
                    "count": "15"
                },
                {
                    "id": "862113",
                    "label": "Penguin Books",
                    "selected": false,
                    "param": "penguin-books",
                    "count": "14"
                },
                {
                    "id": "860933",
                    "label": "Berkeley Books",
                    "selected": false,
                    "param": "berkeley-books",
                    "count": "5"
                },
                {
                    "id": "862253",
                    "label": "Usborne",
                    "selected": false,
                    "param": "usborne",
                    "count": "4"
                },
                {
                    "id": "862172",
                    "label": "Sodis Distribution",
                    "selected": false,
                    "param": "sodis-distribution",
                    "count": "2"
                },
                {
                    "id": "861049",
                    "label": "Công Ty TNHH  MTV  Đầu Tư và Phát Triển Giáo Dục SSG",
                    "selected": false,
                    "param": "công-ty-tnhh--mtv--đầu-tư-và-phát-triển-giáo-dục-ssg",
                    "count": "1"
                },
                {
                    "id": "861691",
                    "label": "Cty XT Sách VN",
                    "selected": false,
                    "param": "cty-xt-sách-vn",
                    "count": "1"
                },
                {
                    "id": "861786",
                    "label": "HarperCollins Publishers",
                    "selected": false,
                    "param": "harpercollins-publishers",
                    "count": "1"
                },
                {
                    "id": "861856",
                    "label": "Interforum Éditis",
                    "selected": false,
                    "param": "interforum-éditis",
                    "count": "1"
                },
                {
                    "id": "861893",
                    "label": "Libri GMBH",
                    "selected": false,
                    "param": "libri-gmbh",
                    "count": "1"
                },
                {
                    "id": "861913",
                    "label": "Mc Graw Hill Book",
                    "selected": false,
                    "param": "mc-graw-hill-book",
                    "count": "1"
                },
                {
                    "id": "862157",
                    "label": "Random House",
                    "selected": false,
                    "param": "random-house",
                    "count": "1"
                }
            ]
        },
        {
            "id": "195",
            "code": "age",
            "label": "Độ Tuổi",
            "param": "độ-tuổi",
            "options": []
        },
        {
            "id": "199",
            "code": "reading_level",
            "label": "Cấp Độ",
            "param": "cấp-độ",
            "options": [{
                    "id": "292799",
                    "label": "Cấp Độ 3",
                    "selected": false,
                    "param": "cấp-độ-3",
                    "count": "2"
                },
                {
                    "id": "292801",
                    "label": "Cấp Độ 1",
                    "selected": false,
                    "param": "cấp-độ-1",
                    "count": "1"
                }
            ]
        },
        {
            "id": "204",
            "code": "languages",
            "label": "Ngôn Ngữ",
            "param": "ngôn-ngữ",
            "options": [{
                "id": "449751",
                "label": "Tiếng Anh",
                "selected": false,
                "param": "tiếng-anh",
                "count": "13"
            }]
        }
    ],
    "total_products": 286,
    "product_list": [{
            "product_id": "333653",
            "product_name": "[Phiên chợ sách cũ] Practical English Usage, 4th edition: Michael Swan's guide to problems in English",
            "product_finalprice": "345.150",
            "product_price": "531.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-practical-english-usage-4th-edition-michael-swan-s-guide-to-problems-in-english.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_193947_thanh_ly.jpg",
            "discount": 35,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">35%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-333653' class='price m-price-font'>345.150đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-333653' class='price m-price-font'>531.000đ</span></p>"
        },
        {
            "product_id": "333230",
            "product_name": "[Phiên chợ sách cũ] Everybody Up 2E Starter: Student Book with Audio CD Pack",
            "product_finalprice": "196.300",
            "product_price": "302.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-everybody-up-2e-starter-student-book-with-audio-cd-pack.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_96728_thanh_ly.jpg",
            "discount": 35,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">35%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-333230' class='price m-price-font'>196.300đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-333230' class='price m-price-font'>302.000đ</span></p>"
        },
        {
            "product_id": "332481",
            "product_name": "[Phiên chợ sách cũ] Everybody Up 2E 2: Student Book",
            "product_finalprice": "169.000",
            "product_price": "260.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-everybody-up-2e-2-student-book.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_195509_1_3645_thanh_ly.jpg",
            "discount": 35,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">35%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-332481' class='price m-price-font'>169.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-332481' class='price m-price-font'>260.000đ</span></p>"
        },
        {
            "product_id": "332346",
            "product_name": "[Phiên chợ sách cũ] IELTS Life Skills Official Cambridge Test Practice B1 Student's Book with Answers and Audio: B1",
            "product_finalprice": "153.400",
            "product_price": "236.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-ielts-life-skills-official-cambridge-test-practice-b1-student-s-book-with-answers-and-audio-b1.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/d/_/d_thanh_ly.jpg",
            "discount": 35,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">35%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-332346' class='price m-price-font'>153.400đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-332346' class='price m-price-font'>236.000đ</span></p>"
        },
        {
            "product_id": "332157",
            "product_name": "[Phiên chợ sách cũ] New Round Up Level 5 Students' Book/CD-ROM Pack",
            "product_finalprice": "188.500",
            "product_price": "290.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-new-round-up-level-5-students-book-cd-rom-pack.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_107470_thanh_ly.jpg",
            "discount": 35,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">35%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-332157' class='price m-price-font'>188.500đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-332157' class='price m-price-font'>290.000đ</span></p>"
        },
        {
            "product_id": "326604",
            "product_name": "[Phiên chợ sách cũ] Cambridge English First 1 For Revised Exam From 2015 Student's Book With Answers Fahasa Edition",
            "product_finalprice": "84.000",
            "product_price": "120.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/phien-cho-sach-cu-cambridge-english-first-1-for-revised-exam-from-2015-student-s-book-with-answers-fahasa-edition.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/9/7/9781107568617_thanh_ly_1.jpg",
            "discount": 30,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">30%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-326604' class='price m-price-font'>84.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-326604' class='price m-price-font'>120.000đ</span></p>"
        },
        {
            "product_id": "116313",
            "product_name": "Richmond Robin Readers Level 2 the Show Must Go on",
            "product_finalprice": "113.050",
            "product_price": "133.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/richmond-robin-readers-level-2-the-show-must-go-on.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_83187.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-116313' class='price m-price-font'>113.050đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-116313' class='price m-price-font'>133.000đ</span></p>"
        },
        {
            "product_id": "116312",
            "product_name": "Richmond Robin Readers Level 2 Ruby's Gift",
            "product_finalprice": "113.050",
            "product_price": "133.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/richmond-robin-readers-level-2-ruby-s-gift.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_83186.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-116312' class='price m-price-font'>113.050đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-116312' class='price m-price-font'>133.000đ</span></p>"
        },
        {
            "product_id": "55695",
            "product_name": "Word Power Made Easy: The Complete Handbook for Building a Superior Vocabulary",
            "product_finalprice": "133.450",
            "product_price": "157.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/word-power-made-easy-the-complete-handbook-for-building-a-superior-vocabulary.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_195509_1_25077.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-55695' class='price m-price-font'>133.450đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-55695' class='price m-price-font'>157.000đ</span></p>"
        },
        {
            "product_id": "116310",
            "product_name": "Richmond Robin Readers Level 1 the Winning Shot",
            "product_finalprice": "102.850",
            "product_price": "121.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/richmond-robin-readers-level-1-the-winning-shot.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_83184.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-116310' class='price m-price-font'>102.850đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-116310' class='price m-price-font'>121.000đ</span></p>"
        },
        {
            "product_id": "36307",
            "product_name": "Your First 100 Words in Vietnamese (Your First 100 Words In...Series)",
            "product_finalprice": "137.700",
            "product_price": "162.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/your-first-100-words-in-vietnamese-your-first-100-words-in-series.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_195509_1_24487.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-36307' class='price m-price-font'>137.700đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-36307' class='price m-price-font'>162.000đ</span></p>"
        },
        {
            "product_id": "116305",
            "product_name": "Richmond Primary Readers Level 6 Footprints in the Forest",
            "product_finalprice": "117.300",
            "product_price": "138.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/richmond-primary-readers-level-6-footprints-in-the-forest.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/9/7/9788466811521.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-116305' class='price m-price-font'>117.300đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-116305' class='price m-price-font'>138.000đ</span></p>"
        },
        {
            "product_id": "145673",
            "product_name": "Time Zones 2 Student Book & Ol Workbook Sticker Code",
            "product_finalprice": "238.000",
            "product_price": "280.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/time-zones-2-student-book-ol-workbook-sticker-code.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_111315.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-145673' class='price m-price-font'>238.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-145673' class='price m-price-font'>280.000đ</span></p>"
        },
        {
            "product_id": "169229",
            "product_name": "KEYNOTE AME 4 WORKBOOK",
            "product_finalprice": "170.000",
            "product_price": "200.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/keynote-ame-4-workbook.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_131026.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169229' class='price m-price-font'>170.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169229' class='price m-price-font'>200.000đ</span></p>"
        },
        {
            "product_id": "137565",
            "product_name": "Survival Vietnamese: Vietnamese Phrasebook : How to Communicate Without Fuss or Fear - Instantly!",
            "product_finalprice": "118.150",
            "product_price": "139.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/survival-vietnamese-vietnamese-phrasebook-how-to-communicate-without-fuss-or-fear-instantly.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_103670.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-137565' class='price m-price-font'>118.150đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-137565' class='price m-price-font'>139.000đ</span></p>"
        },
        {
            "product_id": "169227",
            "product_name": "KEYNOTE AME 2 WORKBOOK",
            "product_finalprice": "170.000",
            "product_price": "200.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/keynote-ame-2-workbook.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_131024.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169227' class='price m-price-font'>170.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169227' class='price m-price-font'>200.000đ</span></p>"
        },
        {
            "product_id": "137128",
            "product_name": "Pocket Vietnamese Dictionary",
            "product_finalprice": "202.300",
            "product_price": "238.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/pocket-vietnamese-dictionary-137342.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_103871.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-137128' class='price m-price-font'>202.300đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-137128' class='price m-price-font'>238.000đ</span></p>"
        },
        {
            "product_id": "169226",
            "product_name": "KEYNOTE AME 1 WORKBOOK",
            "product_finalprice": "170.000",
            "product_price": "200.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/keynote-ame-1-workbook.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_131023.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169226' class='price m-price-font'>170.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169226' class='price m-price-font'>200.000đ</span></p>"
        },
        {
            "product_id": "128629",
            "product_name": "Amazing Philanthropists : B1",
            "product_finalprice": "126.650",
            "product_price": "149.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/amazing-philanthropists-b1.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_113341.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-128629' class='price m-price-font'>126.650đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-128629' class='price m-price-font'>149.000đ</span></p>"
        },
        {
            "product_id": "169208",
            "product_name": "IMPACT 3 - WORKBOOK",
            "product_finalprice": "170.000",
            "product_price": "200.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/impact-3-workbook.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_131005.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169208' class='price m-price-font'>170.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169208' class='price m-price-font'>200.000đ</span></p>"
        },
        {
            "product_id": "128627",
            "product_name": "Amazing Composers : A2-B1",
            "product_finalprice": "126.650",
            "product_price": "149.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/amazing-composers-a2-b1.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_113348.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-128627' class='price m-price-font'>126.650đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-128627' class='price m-price-font'>149.000đ</span></p>"
        },
        {
            "product_id": "169207",
            "product_name": "Impact: Workbook 2",
            "product_finalprice": "170.000",
            "product_price": "200.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": "0",
            "product_url": "https://www.fahasa.com/impact-2-workbook.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/8/9/8934986097599-2.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169207' class='price m-price-font'>170.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169207' class='price m-price-font'>200.000đ</span></p>"
        },
        {
            "product_id": "128624",
            "product_name": "Amazing Mathematicians : A2-B1",
            "product_finalprice": "126.650",
            "product_price": "149.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/amazing-mathematicians-a2-b1.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_113342.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-128624' class='price m-price-font'>126.650đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-128624' class='price m-price-font'>149.000đ</span></p>"
        },
        {
            "product_id": "169206",
            "product_name": "IMPACT 1 - STUDENT BOOK",
            "product_finalprice": "238.000",
            "product_price": "280.000",
            "rating_html": "<div class='ratings'>\n\t\t\t<div class='rating-box'>\n\t\t\t    <div class='rating' style='width:0%'></div>\n\t\t\t</div>\n\t\t    <div class='amount'>(0)</div>\n\t\t</div>",
            "soon_release": null,
            "product_url": "https://www.fahasa.com/impact-1-student-book.html",
            "image_src": "https://cdn0.fahasa.com/media/catalog/product/cache/1/small_image/400x400/9df78eab33525d08d6e5fb8d27136e95/i/m/image_131003.jpg",
            "discount": 15,
            "discount_label_html": "<div class=\"label-pro-sale m-label-pro-sale\"><span class=\"p-sale-label discount-l-fs\">15%</span></div>",
            "price_html": "<p class='special-price'><span class='price-label'>Special Price</span><span id='product-price-169206' class='price m-price-font'>238.000đ</span></p><p class='old-price bg-white'><span class='price-label'>Giá bìa: </span><span id='old-price-169206' class='price m-price-font'>280.000đ</span></p>"
        }
    ],
    "noofpages": 12
}
var $jq = jQuery.noConflict();
const CatalogAjax = function() {
    var $this = this;
    $parent_category = $jq("#parent-category");
    $current_category = $jq("#current-category");
    $children_category = $jq("#children-categories");
    var default_show_limit = 8;
    this.CatalogDataProcess = function(data) {
        $this.displayCategory(data['category'], data['parent_categories'], data['children_categories']);
        // $this.displayPrice(data['price_range']);
        // if (data['total_products'] > 0) {
        //     $this.displayAttributes(data['attributes']);
        //     $jq(".button-open-category-mobile.clone-filter").hide();
        // } else {
        //     if (mobile == 1) {
        //         $jq(".button-open-category-mobile.clone-filter").show();
        //     }
        //     $this.displayFilter();
        // }
        // $this.page_total = Math.ceil(data['total_products'] / $this.limit);
        // $this.displayProduct(data['product_list']);
        try { $jq('#parent-category li a').click(function(e) { e.preventDefault(); return false; }); } catch (ex) {}
        try { $jq('#current-category li a').click(function(e) { e.preventDefault(); return false; }); } catch (ex) {}
        try { $jq('#children-categories li a').click(function(e) { e.preventDefault(); return false; }); } catch (ex) {}
        // $this.hideLoadingAnimation();
        // neu co show thi tat modal
        //        if($jq('#modal-filter-mobile').hasClass('in')){
        //            $jq('#modal-filter-mobile').modal('hide');
        //        }
    }

    this.hello = function() {
        console.log(data);
    }

    this.displayCategory = function(current_categories, parent_categories, childrent_categories) {
        var parrent_cat = "";
        var current_cat = "";
        var childrent_cat = "";
        var last_parrent_index = 0;
        var current_is_in_parrent = false;


        if ($current_category.hasClass("parent-current-category")) {
            $current_category.removeClass("parent-current-category")
        }
        if (parent_categories.length == 1) { // vi chi = 0 thi` no la cha cua tat ca
            if (current_categories['name'] == parent_categories[0]['name']) {
                $current_category.addClass("parent-current-category");
                $current_category.removeAttr('style');
            }
        }
        if (parent_categories[parent_categories.length - 1]['id'] == current_categories['id']) {
            // phan loai cap trong current_categories moi con cua no margin left +10 px;
            $current_category.removeAttr('style');
            let num = ((parent_categories.length - 1) * 10) + 'px';
            $current_category.css("margin-left", num);

        }
        Object.keys(parent_categories).forEach(function(key) {
            if (parent_categories[key].id == current_categories.id) {
                current_is_in_parrent = true;
            } else {
                last_parrent_index = key;
            }
        });
        // phan loai cap trong parent_categories moi con cua no margin left +10 px;
        var demParent = 0;
        var numberMarLeft = 0;
        Object.keys(parent_categories).forEach(function(key) {
            if (parent_categories[key].id != current_categories.id) {
                if (!current_is_in_parrent && (last_parrent_index == key)) {
                    return;
                }
                if (demParent > 0) {
                    // tu index 1 tro len moi margin-left vi 0 la cha cua tat ca
                    numberMarLeft += 10;
                    parrent_cat += "<li style='margin-left:" + numberMarLeft + "px;'><a href='" + parent_categories[key].url + "' cat_id='" + parent_categories[key].id + "' onclick='catalog_ajax.category_click(this)' title='" + parent_categories[key].name + "'>" + parent_categories[key].name + "</a></li>";
                } else {
                    parrent_cat += "<li><a href='" + parent_categories[key].url + "' cat_id='" + parent_categories[key].id + "' onclick='catalog_ajax.category_click(this)' title='" + parent_categories[key].name + "'>" + parent_categories[key].name + "</a></li>";
                }
            }
            demParent++;
        });

        if (!current_is_in_parrent) {
            current_cat = "<a href='" + parent_categories[last_parrent_index].url + "' cat_id='" + parent_categories[last_parrent_index].id + "' onclick='catalog_ajax.category_click(this)' title='" + parent_categories[last_parrent_index].name + "'>" + parent_categories[last_parrent_index].name + "</a>";
        } else {
            current_cat = "<span class='m-selected-filter-item'>" + current_categories.name + "</span>";
        }

        var is_expand_category = false;
        var current_cat_index = 0;
        Object.keys(childrent_categories).forEach(function(key) {
            if (childrent_categories[key].count > 0) {
                if (childrent_categories[key].id != current_categories.id) {
                    current_cat_index++;
                    childrent_cat += "<li><a href='" + childrent_categories[key].url + "' cat_id='" + childrent_categories[key].id + "' onclick='catalog_ajax.category_click(this)' title='" + childrent_categories[key].name + "'>" + childrent_categories[key].name + "</a> (" + childrent_categories[key].count + ")</li>";
                } else {
                    if (!is_expand_category && (current_cat_index >= default_show_limit)) { is_expand_category = true; }
                    current_cat_index++;
                    childrent_cat += "<li><span class='m-selected-filter-item'>" + childrent_categories[key].name + "</span> (" + childrent_categories[key].count + ")";
                }
            }
        });
        let numLeft = (numberMarLeft + 10) + 'px';
        // $this.displaySEO(current_categories);
        // $this.displayBreadcrumb(current_categories, parent_categories);
        $parent_category.html(parrent_cat);
        $current_category.html(current_cat);
        $children_category.html(childrent_cat);
        $children_category.css('margin-left', numLeft);
        (function($) { $(document).trigger('m-show-more-reset', ['left_category', default_show_limit, false, 200, 0]); })(jQuery);
        (function($) { $(document).trigger('m-show-more-reset', ['left_category', default_show_limit, is_expand_category, 200, 0]); })(jQuery);
    };

}
var catalog = new CatalogAjax();
catalog.hello("Luan Nguyen");
catalog.CatalogDataProcess(data);